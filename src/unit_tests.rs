use super::*;


#[test]
fn input_hello_will_output_2315313134() {
    assert_eq!("2315313134", encrypt_with_polybius_square("hello"));
    assert_eq!("2315313134", encrypt_with_polybius_square("HELLo"));
}


#[test]
fn non_letters_are_not_changed_to_numbers() {
    assert_eq!("' '1234567890`_+'", encrypt_with_polybius_square("' '1234567890`_+'"));
}
