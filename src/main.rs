#[cfg(test)]
mod unit_tests;

pub fn encrypt_with_polybius_square(string : &str) -> String {
    let mut new_string = String::new();
    for character in string.chars() {
        let index = letter_index_in_alphabet(character);
        match index {
            Some(mut idx) => {
                idx -= if idx >= 10 { 1 } else { 0 }; 
                let row = idx / 5 + 1;
                let col = idx % 5 + 1;
                new_string.push_str(&format!("{}{}", row, col));
            },
            None => new_string.push(character)
        }
    }
    new_string
}


fn letter_index_in_alphabet(letter : char) -> Option<u32> {
    if !is_letter(letter) {
        None
    } else {
        Some(
            letter as u32 - if letter as u32 >= 'a' as u32  {
                'a' as u32
            } else {
                'A' as u32
            })
    }
}


fn is_letter(character: char) -> bool {
    match character {
        'A'...'Z' | 'a'...'z' => true,
        _ => false
    }
}


fn main() {
    println!("Hello, world!");
}
